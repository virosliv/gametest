#include "stdafx.h"
#include <iostream>
#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(1200,800), "Lesson 1. kychka-pc.ru");
//=================================================================	
	sf::Image image;
	image.loadFromFile("images/hero.png");

	sf::Texture texture;
	texture.loadFromImage(image);

	sf::Sprite sprite;
	sprite.setTexture(texture);

	int Hspr = 806;
	int Wspr = 375;
	sprite.setTextureRect(sf::IntRect(0,0,Wspr,Hspr));
	sprite.setPosition(0,40); 
////=================================================================	
//	sf::Image * imageArr = new sf::Image[4];
//	imageArr[0].loadFromFile("images/stay0.png");
//	imageArr[1].loadFromFile("images/stay1.png");
//	imageArr[2].loadFromFile("images/stay2.png");
//	imageArr[3].loadFromFile("images/stay3.png");
//
//	sf::Texture * textureArr = new sf::Texture[4];
//	for (int i = 0 ; i < 4 ; i ++)
//		textureArr[i].loadFromImage(imageArr[i]);
//
//
//	sf::Sprite * spriteArr =  new sf::Sprite[4];
//	for (int i = 0 ; i < 4 ; i ++)
//	{
//		spriteArr[i].setTexture(textureArr[i]);
//		spriteArr[i].setPosition(200,30);
//	}
	
	float CurrentFrame = 0;
	
	while (window.isOpen())
	{
		sf::Event event;

		sf::Clock clock;

		while (window.pollEvent(event))
		{
			float time = clock.getElapsedTime().asMicroseconds(); //set past time
			clock.restart(); // restart time
			time /= 800.; //speed game

			std::cout<< "time: " << time << "\n";

			if (event.type == sf::Event::Closed ||
				(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
				window.close();
//==================================================================================================================================	
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ||
				sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				sprite.move(0.1 * time,0);
				sprite.setTextureRect(sf::IntRect(0 ,Hspr*2,Wspr ,Hspr*3));
				std::cout << "Right\n";
			}

//==================================================================================================================================	
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ||
				sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				CurrentFrame += 0.005 * time;
				if(CurrentFrame > 8) CurrentFrame -= 8;
				sprite.setTextureRect(sf::IntRect(Wspr * int(CurrentFrame), Hspr*3, Wspr, Hspr*4));
				sprite.move(-0.1 * time,0);
				std::cout << "Left\n";
			}

//==================================================================================================================================	
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ||
				sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				sprite.setTextureRect(sf::IntRect(0 ,Hspr*0,Wspr ,Hspr*1));
				std::cout << "Up\n";
			}

//==================================================================================================================================	
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				sprite.setTextureRect(sf::IntRect(0 ,Hspr*1,Wspr,Hspr*2));
				std::cout << "Down\n";
			}


//==================================================================================================================================	
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{ 
				sprite.setColor(sf::Color::Red);
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
			{
				sprite.setColor(sf::Color::White);
			}
		}
 
		window.clear();

		window.draw(sprite);
		//for (int i = 0 ; i < 4 ; i ++)
		//{
		//	window.clear();window.draw(sprite);
		//	window.draw(spriteArr[i]);
		//	
		//}
		window.display();
	}
 
	return 0;
}